FROM rust:latest

RUN apt-get update
RUN apt-get install -yqq libgtk-3-dev libjack-jackd2-dev
RUN rustup component add clippy rustfmt
RUN cargo install cargo-criterion
RUN cargo install cargo-prune
