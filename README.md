# MF Docker

Docker environment for running musical fruit products.

## Deployment

### Login

Login requires an access token with `api`, `read_registry` and `write_registry` access.

```shell
docker login registry.gitlab.com -u $USERNAME -p $ACCESS_TOKEN
```

### Build and Upload

```shell
docker build -t registry.gitlab.com/musical-fruit/docker/mf .
docker push registry.gitlab.com/musical-fruit/docker/mf
```
